# docs-wirvsvirus

Documentation of the corona-connect project initiated at WirVsVirus Hackathon 2020.

[[_TOC_]]

## Inspiration
Während der COVID-19 Pandemie wird Risikogruppen und älteren Menschen geraten Zuhause zu bleiben. Das führt allerdings dazu, dass sie Hilfe brauchen (zum Beispiel beim Einkaufen). Aber nicht nur das, viele haben keinen oder schlechten Zugang zum Internet und den dortigen Informationen. Einige fühlen sich einsam.
In Gesprächen mit unseren Großeltern haben wir während des Hackathons gelernt, wie wichtig es ihnen ist direkt mit einem echten Menschen sprechen zu können.

Gleichzeitig hat die junge Generation oft nichts zu tun. So war die Idee für COCO - corona connect geboren. Eine Brücke zwischen den älteren Mitbürgern und der jungen Generation. Per Telefon mit direktem Feedback.

## Was es tut
Für die ältere Generation und Risikogruppen bieten wir eine Hotline (momentan +1 956 247 4513). Dort gibt man die eigene Postleitzahl an und wird mit einem freiwilligen Helfer aus der Nähe direkt verbunden. So hat man direkt einen echten Menschen am Telefon.

Für junge Menschen liefern wir eine Möglichkeit schnell und einfach vor Ort zu helfen. Man meldet sich an, bestätigt die eigene Telefonnummer und schon ist man Teil der Helferdatenbank. Wer eine Pause braucht, kann sich einfach über die Webapp deaktivieren.

## Wie wir es umgesetzt haben
We are building a vue.js frontend hosted with now. Our backend is serverless, written in python and hostet on AWS.

For building the hotline flow we won the support by Twilio and are using their system Twilio Flex.

## Challenges we ran into
Now, how do we build a hotline? On Friday afternoon we had no idea. Luckily we were able to solve this by winning over supporters. Especially the people at Twilio were extremely helpful.

## Accomplishments that we're proud of
- We have a live hotline at: +1 956 247 4513

## What we learned
- It is also a lot about having someone to talk to and for getting information. It's not always about actually doing something.

## What's next for Corona Connect
We need helpers!
- To do this we want to create small viral loops into our webapp, so helpers can share this more often
- We also want to integrate a reward/gamification system for helpers for extra motivation (e.g. get a steam card)

## Long story short

### Challenges we ran into

#### We are not alone

### Accomplishments that we're proud of

#### Amazing API!

#### Amazing Frontend!

#### Amazing Teamwork!

### What we learned

#### Question your own idea

#### Twilio API (wow)

### What's next for Corona Connect

#### Reliability

#### Trustworthiness

#### Scaling with real people

#### Corona is over, and CoCo?

## The CoCo-Log

### Freitag

#### Team building

#### Powerpoint-engineers

<!-- 
Probably breaking the world record for the fastest project outline and best teamwork, the whole CoCo team merges their imaginations of CoronaConnect into this [slideshow](https://docs.google.com/presentation/d/18xIhT8xeUoSESRuxeefW20YHk94bjkPjQTCRGPtkS7Q/edit#slide=id.g7f47116846_6_22).
-->

In Weltrekordzeit vereinigen alle 10 frisch verliebten Mitglieder des CoCo-Teams ihre Vorstellungen von CoronaConnect in einer Slideshow, die sich sehen lassen kann! Den Link findet ihr [hier](https://docs.google.com/presentation/d/18xIhT8xeUoSESRuxeefW20YHk94bjkPjQTCRGPtkS7Q/edit#slide=id.g7f47116846_6_22).


#### CoCo 0.1 - The grocery croudsourcing service

<!-- 
Imagine you would possibly risk your life getting infected outside. Most essential to being able to staying inside is not having to worry about food. So what if the younger generation helped out elderly people to do their groceries? Sounds great, but first of all, how would the elderly people contact any volunteers whose existence they did not know about before? It has to be the telephone.
-->



#### The telephone

<!-- 
Building a croudsourcing hotline service sounds hard, and especially regarding the programming of automated caller-matching we have yet to get proficient in. [MISSING]
-->

#### Trilio support

<!-- 
[MISSING] Therefore being supported by the amazing people at Trilio is really a blessing.
-->

Gerade in dieser Jahreszeit ist die Zufuhr von Vitaminen besonders wichtig. Wir haben das Glück, unsere Kontakte bei Twilio von unserer Idee zu überzeugen, der Ressource Telefon (und vielem, vielem mehr) steht somit nichts mehr im Weg!

#### Software-Architektur

Mittlerweile wissen wir ungefähr

### Samstag

<!-- 
So we have decided on an architecture of our backend for our MVP. [MISSING] 

Rethinking the whole grocery-shopping idea, there seem to be way too many open questions, starting with the payment method. If CoCo means to be the uttermost accessible service we want it to be, secure payment in terms of everything but online-banking sure poses a challenge. In general, we will need to prove that we are a trustworthy service, especially considering the healthy scepsis we know from our grandparents. Following a doubtful silence in our Discord channel, we decide to get first-hand information.
-->

#### Calling grandma

<!-- 
To be honest, we receive mixed feedback conducting our mini-survey. The general response to connecting otherwise isolated people is very positive, but clearly our idea needs refinement om terms of

- Trustworthiness
- [MISSING]

Also the majority of our grandparents can imagine that a lot of people would benefit from our idea but they are actually either mobile enough (sometimes too mobile with respect to the current crisis) to manage things by themselves or are already getting help from their family, town community etc. Of course you would rather call your grandchildren than a random stranger! That is when we detect a serious bias in our survey: We do not know the people yet who would gladfully accept support of caring volunteers. We really need to focus on spreading the word about CoronaConnect.

> Shout out to all grandparents and grandparents' friends for their insightful and honest information!
-->

#### User Journey

Im Anschluss an die Gespräche mit unseren Großeltern entwickeln wir eine User Journey. Es ist wichtig, dass die Hilfsbedürftigen einfach und unkompliziert verbunden werden. Als Freiwillige*r möchte man gebraucht, aber auch nicht von der Nachfrage überwältigt werden. Die komplette User Story findet ihr [hier](https://drive.google.com/drive/folders/1rjFTG8o54TOScvfA0zyw4rlyRjUrqC7i).

#### CoCo 0.2 - The empathic croudsourcing hotline


#### CoCo 0.3 - The hot-wire to your neighbourhood

### Sonntag

#### Authentifizierung

#### Die Frontend-Rakete

# Englisch

## Inspiration
During the COVID-19 pandemic people from risk groups and the elderly are advised to stay at home. This leads to them requiring help, lacking access to information online and feeling lonely. During interviews conducted with those people during the hackathon, we learned how important it is to talk to real people immediately.
At the same time our younger generation is left with less to do and boredom. That is how the idea for building a bridge between them was born: COCO - corona connect.

## What it does
We offer a hotline at +1 956 247 4513 that the elderly can call. They say their zip code out loud and based on that are connected to a volunteer that lives closest to them.
To sign up, volunteers visit use our webapp. There they enter their information, verify their phone number and can change their availability status at any time.